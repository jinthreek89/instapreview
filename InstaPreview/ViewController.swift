//
//  ViewController.swift
//  InstaPreview
//
//  Created by mojave on 12/25/18.
//  Copyright © 2018 mojave. All rights reserved.
//

import UIKit

class ViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    fileprivate lazy var userDefault = UserDefaults.standard
    fileprivate lazy var images: [String] = []
    fileprivate lazy var customImages: [Int: Any] = [:]
    fileprivate var selectedIndexPath: IndexPath? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let instaUsername = userDefault.value(forKey: "username") as? String {
            callRequest(with: instaUsername)
        } else {
            promptAccountName()
        }
    }
    
    fileprivate func promptAccountName() {
        let alertVC = UIAlertController(title: "Account name", message: "Enter your instagram account name", preferredStyle: .alert)
        alertVC.addTextField { (textfield) in
            
        }
        let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
            if let username = alertVC.textFields?.first?.text {
                self.userDefault.set(username, forKey: "username")
                self.callRequest(with: username)
            } else {
                
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertVC.addAction(okAction)
        alertVC.addAction(cancelAction)
        present(alertVC, animated: true, completion: nil)
    }

    fileprivate func callRequest(with username: String) {
        let parameters: [String: Any] = [
            "account": username,
            "type": "instagram"
        ]
        
        guard let url = URL(string: "http://13.229.76.232/api/image") else { return }
        
        let session = URLSession.shared
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
            return
        }
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        title = "Loading..."
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            self.title = "Error"
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    print(json)
                    if let success = json["success"] as? Bool {
                        if success {
                            self.title = "Result"
                            if let imgs = json["images"] as? [String] {
                                self.images = imgs
                                DispatchQueue.main.async {
                                    self.collectionView.reloadData()
                                }
                            }
                            return
                        }
                    }
                }
            } catch let error {
                print(error.localizedDescription)
            }
        })
        task.resume()

    }
    
    @IBAction func onPressedAccount(_ sender: Any) {
        promptAccountName()
    }
    
    @IBAction func onPressedRefresh(_ sender: Any) {
        self.images.removeAll()
        self.customImages.removeAll()
        self.collectionView.reloadData()
        if let instaUsername = userDefault.value(forKey: "username") as? String {
            callRequest(with: instaUsername)
        }
    }
    
    // MARK: - UICollectionView datasource
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.images.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ImageCollectionViewCell
        if let data = customImages[indexPath.row] {
            if let image = data as? UIImage {
                cell.imageView.image = image
            } else {
                cell.imageView.image = UIImage(named: "instagram-logo")
            }
            cell.setBorder(true)
        } else {
            cell.setImageUrl(images[indexPath.row])
            cell.setBorder(false)
        }
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let actionSheet = UIAlertController(title: "Select Photo", message: nil, preferredStyle: .actionSheet)
        
        if let _ = customImages[indexPath.row] as? UIImage {
            let removeAction = UIAlertAction(title: "Remove", style: .destructive) { (action) in
                self.customImages.removeValue(forKey: indexPath.row)
                self.collectionView.reloadItems(at: [indexPath])
            }
            actionSheet.addAction(removeAction)
        }
        
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (action) in
            self.presentPicker(.camera)
        }
        let photoAction = UIAlertAction(title: "Photo Album", style: .default) { (action) in
            self.presentPicker(.photoLibrary)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { action in
            self.selectedIndexPath = indexPath
        }
        actionSheet.addAction(cameraAction)
        actionSheet.addAction(photoAction)
        actionSheet.addAction(cancelAction)
        present(actionSheet, animated: true, completion: nil)
        selectedIndexPath = indexPath
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenWidth = UIScreen.main.bounds.width
        let w = (screenWidth - 2 * 4) / 3
        return CGSize(width: w, height: w)
    }
    
    private func presentPicker(_ type: UIImagePickerController.SourceType) {
        let vc = UIImagePickerController()
        vc.sourceType = type
        vc.allowsEditing = true
        vc.delegate = self
        present(vc, animated: true)
    }
}

extension ViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
        self.selectedIndexPath = nil
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        
        guard let image = info[.editedImage] as? UIImage else {
            print("No image found")
            return
        }
        guard let indexPath = selectedIndexPath else {
            return
        }
        customImages[indexPath.row] = image
        self.collectionView.reloadItems(at: [indexPath])
        
        self.selectedIndexPath = nil
    }
}
