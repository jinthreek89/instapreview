//
//  ImageCollectionViewCell.swift
//  InstaPreview
//
//  Created by mojave on 12/25/18.
//  Copyright © 2018 mojave. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imageView.layer.borderColor = UIColor.red.cgColor
    }
    
    func setImageUrl(_ urlString: String) {
        guard let url = URL(string: urlString) else { return }
        setImageUrl(url)
    }
    
    func setImageUrl(_ url: URL) {
        let processor = DownsamplingImageProcessor(size: imageView.bounds.size)
            >> RoundCornerImageProcessor(cornerRadius: 0)
        imageView.kf.indicatorType = .activity
        imageView.kf.setImage(
            with: ImageResource(downloadURL: url),
            placeholder: UIImage(named: "instagram-logo"),
            options: [
                .processor(processor),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage
            ])
        {
            result in
            switch result {
            case .success(let value):
                print("Task done for: \(value.source.url?.absoluteString ?? "")")
            case .failure(let error):
                print("Job failed: \(error.localizedDescription)")
            }
        }
    }
    
    func setBorder(_ v: Bool) {
        if v {
            imageView.layer.borderWidth = 1
        } else {
            imageView.layer.borderWidth = 0
        }
    }
}
